resource "azurerm_resource_group" "sample" {
  name     = var.resource_group_name
  location = var.resource_group_location
}

output "resourceGroup" {
    value=var.resource_group_name
}

output "workspace" {
    value="${terraform.workspace}"
}

# module "vm" {
#   source              = "../vm"
#   name                = var.vm_name
#   location            = azurerm_resource_group.example.location
#   resource_group_name = azurerm_resource_group.example.name
#   subscription_id     = var.subscription_id
# }