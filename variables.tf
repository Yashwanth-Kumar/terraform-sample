variable "subscription_id" {
  description = "Subscription ID."
  type        = string
}

variable "client_id" {
  description = "Client ID."
  type        = string
}

variable "client_secret" {
  description = "Client secret"
  type        = string
}

variable "tenant_id" {
  description = "tenant ID."
  type        = string
}

variable "resource_group_name" {
  description = "resource group name"
  type        = string
}

variable "resource_group_location" {
  default     = "eastus"
  description = "Name of the region"
  type        = string
}

